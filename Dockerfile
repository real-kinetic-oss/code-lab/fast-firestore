# Dockerfile
FROM python:3.12-alpine

# Install build dependencies
RUN apk add --no-cache gcc g++ musl-dev libffi-dev openssl-dev

COPY . /src
WORKDIR /src

RUN pip install -r requirements.txt 

EXPOSE 8080
CMD ["python", "-m", "src.main"]
