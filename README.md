# Fast Firestore
This repo hosts a [FastAPI](https://fastapi.tiangolo.com/) server that interacts with [Firestore](https://cloud.google.com/firestore/docs/overview). 
It is deployed to GCP with [Cloud Run](https://cloud.google.com/run/docs/overview/what-is-cloud-run)


This repo is a companion to our [Shippping Fast with FastAPI and Cloud Run]()

