import json
import logging
import os
import sys

from fastapi import FastAPI, HTTPException, Response, status
from pydantic import BaseModel, Field

from googleapiclient import discovery
from googleapiclient.errors import HttpError
from google.cloud import firestore
import uvicorn


# Add structured logging
logging.basicConfig(level=logging.INFO, handlers=[logging.StreamHandler(sys.stdout)])
logger = logging.getLogger("firestore_database")


app = FastAPI(title="Medium Blog API", docs_url="/api/docs")

# Create a discovery client for interacting with the Firestore API
client = discovery.build("firestore", "v1")

# Environment variable is mounted in the container
firestore_ = firestore.Client(database=os.getenv("DATABASE_NAME"))


class PostRequest(BaseModel):
    project: str
    location: str = Field(default="nam5")
    type: str = Field(default="FIRESTORE_NATIVE")
    database: str = Field(default="(default)")


class PutRequest(BaseModel):
    description: str
    rating: int


@app.get(
    "/items/{item_id}",
    status_code=status.HTTP_200_OK,
    description="Get an item by item_id.",
)
async def get_item(item_id: str) -> dict:
    doc_ref = firestore_.collection("my-collection").document(item_id)

    doc = doc_ref.get().to_dict()

    if not doc:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"The item {item_id} does not exist.",
        )

    return doc


@app.post(
    "/database",
    status_code=status.HTTP_200_OK,
    description="Creates a new firestoe database",
)
async def create_db(request: PostRequest) -> dict:
    db_name = f"projects/{request.project}/databases/{request.database}"
    firestore_db = {}

    # Check if database already exists
    try:
        firestore_db = client.projects().databases().get(name=db_name).execute()

    except HttpError as error:
        if json.loads(error.content).get("error", {}).get("status") != "NOT_FOUND":
            raise error

        logger.info(f"Creating new db {db_name}")

    if firestore_db:
        logger.info(f"Database already exists.")
        return firestore_db

    # Create the db
    try:
        operation = (
            client.projects()
            .databases()
            .create(
                parent=f"projects/{request.project}",
                databaseId=request.database,
                body={"type": request.type, "location_id": request.location},
            )
            .execute()
        )
    except HttpError as error:
        raise error

    return operation.get("response", {})


@app.put(
    "/items/{item_id}",
    status_code=status.HTTP_201_CREATED,
    description="Create or update item",
)
def upsert_item(item_id: str, request: PutRequest, response: Response) -> dict:
    doc_ref = firestore_.collection("my-collection").document(item_id)

    doc = doc_ref.get().to_dict()

    if doc == request.model_dump():
        response.status_code = status.HTTP_200_OK
        return request.model_dump()

    doc_ref.set(request.model_dump())

    updated_doc = doc_ref.get().to_dict()

    return updated_doc


if __name__ == "__main__":
    uvicorn.run("src.main:app", host="0.0.0.0", port=8080)
